/**
 * generated by Xtext 2.30.0
 */
package org.xtext.pascalDSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>procedure heading</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.pascalDSL.PascalDSLPackage#getprocedure_heading()
 * @model
 * @generated
 */
public interface procedure_heading extends abstraction_heading
{
} // procedure_heading
