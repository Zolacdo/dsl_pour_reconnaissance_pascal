/**
 * generated by Xtext 2.30.0
 */
package org.xtext.pascalDSL;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>enumerated type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.pascalDSL.enumerated_type#getIdentifiers <em>Identifiers</em>}</li>
 * </ul>
 *
 * @see org.xtext.pascalDSL.PascalDSLPackage#getenumerated_type()
 * @model
 * @generated
 */
public interface enumerated_type extends EObject
{
  /**
   * Returns the value of the '<em><b>Identifiers</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Identifiers</em>' containment reference.
   * @see #setIdentifiers(identifier_list)
   * @see org.xtext.pascalDSL.PascalDSLPackage#getenumerated_type_Identifiers()
   * @model containment="true"
   * @generated
   */
  identifier_list getIdentifiers();

  /**
   * Sets the value of the '{@link org.xtext.pascalDSL.enumerated_type#getIdentifiers <em>Identifiers</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Identifiers</em>' containment reference.
   * @see #getIdentifiers()
   * @generated
   */
  void setIdentifiers(identifier_list value);

} // enumerated_type
